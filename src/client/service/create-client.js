const sns = require("ebased/service/downstream/sns");
const dynamo = require('ebased/service/storage/dynamo');

const createClient = async (item) => {
  try {
    await dynamo.putItem({ TableName: process.env.CLIENTS_TABLE, Item: item});
  } catch (e) {
    console.error(e)
  }
};

const publishCreateCardAndGift = async (clientCreatedEvent) => {
  const { eventPayload, eventMeta } = clientCreatedEvent.get();
  const snsPublishParams = {
    TopicArn: process.env.SNS_TOPIC_ARN,
    Message: eventPayload
  };
  await sns.publish(snsPublishParams, eventMeta);
};

module.exports = { publishCreateCardAndGift, createClient };
