const dynamo = require('ebased/service/storage/dynamo');

const saveCard = async (dni, card) => {
  try {
    const dbParams = {
      TableName: process.env.CLIENTS_TABLE,
      UpdateExpression: "SET #card = :card",
      Key: { dni: dni},
      ExpressionAttributeNames: {
        "#card": "creditCard",
      },
      ExpressionAttributeValues: {
        ":card": {
            "number": card.number,
            "expiration": card.expiration,
            "ccv": card.ccv,
            "type": card.type
        }
      }
    };
    await dynamo.updateItem(dbParams);
  } catch (e) {
    console.error(e)
  }
}

module.exports = { saveCard };
