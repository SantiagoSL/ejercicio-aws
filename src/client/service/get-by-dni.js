const dynamo = require('ebased/service/storage/dynamo');

const getByDni = async (commandPayload) => {
  const { Items } = await dynamo.queryTable({
    TableName: process.env.CLIENTS_TABLE,
    KeyConditionExpression: 'dni = :dni',
    ExpressionAttributeValues: { ':dni': commandPayload.dni },
  });
  const result = Items.length > 0 ? Items[0] : null;
  return result;
}

module.exports = { getByDni };