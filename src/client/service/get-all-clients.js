const dynamo = require('ebased/service/storage/dynamo');

const getAllClients = async () => {
  const { Items } = await dynamo.scanTable({
    TableName: process.env.CLIENTS_TABLE,
    ExpressionAttributeNames: {
      '#d': 'dni',
      '#n': 'name'
    },
    ProjectionExpression: "#d, lastName, #n, birthDate",
  });
  return Items;
}

module.exports = { getAllClients };
