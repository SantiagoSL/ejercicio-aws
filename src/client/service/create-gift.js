const dynamo = require('ebased/service/storage/dynamo');

const saveGift = async (dni, gift) => {
  try {
    console.log(gift)
    const dbParams = {
      TableName: process.env.CLIENTS_TABLE,
      UpdateExpression: "SET  #G = :G",
      Key: { dni: dni },
      ExpressionAttributeNames: {
        "#G": "gift",
      },
      ExpressionAttributeValues: {
        ":G": gift
      }
    };
    await dynamo.updateItem(dbParams);
  } catch (e) {
    console.error(e)
  }
}

module.exports = { saveGift };
