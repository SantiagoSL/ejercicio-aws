const dynamo = require('ebased/service/storage/dynamo');

const updateClient =  async (commandPayload) => {
  const dbParams = {
    TableName: process.env.CLIENTS_TABLE,
    UpdateExpression: "SET",
    Key: { dni: commandPayload.dni },
    ExpressionAttributeNames: {
    },
    ExpressionAttributeValues: {
    },
  }
  Object.keys(commandPayload).forEach(key => {
    if(key !== 'dni') {
      dbParams.UpdateExpression += ` #${key}=:${key},`;
      dbParams.ExpressionAttributeNames[`#${key}`] = key;
      dbParams.ExpressionAttributeValues[`:${key}`] = commandPayload[key];
    }
  });
  dbParams.UpdateExpression = dbParams.UpdateExpression.slice(0, -1);
  await dynamo.updateItem(dbParams);
}

module.exports = { updateClient };