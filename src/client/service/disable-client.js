const dynamo = require('ebased/service/storage/dynamo');

const disableClient = async (dni) => {
  const dbParams = {
    TableName: process.env.CLIENTS_TABLE,
    UpdateExpression: "SET  #D = :D",
    Key: { dni: dni },
    ExpressionAttributeNames: {
      "#D": "disabled",
    },
    ExpressionAttributeValues: {
      ":D": true
    }
  };
  await dynamo.updateItem(dbParams);
}

module.exports = { disableClient };