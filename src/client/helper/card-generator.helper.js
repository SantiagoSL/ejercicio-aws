function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function generateCardNumber() {
  return `${getRandomInt(0, 9999)}-${getRandomInt(0, 9999)}-${getRandomInt(0, 9999)}`
}

function generateCcv() {
  return `${getRandomInt(0, 1000)}`;
}

function generateExpirationDate() {
  return `${getRandomInt(1, 32)}/${getRandomInt(1, 13)}`
}


function generateCard() {
  return {
    number: generateCardNumber(),
    ccv: generateCcv(),
    expiration: generateExpirationDate()
  }
}

module.exports = { generateCard };
