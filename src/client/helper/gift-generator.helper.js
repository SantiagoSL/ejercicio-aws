function isAutumn(birthDate) {
  const dt = new Date(birthDate);
  const m = dt.getMonth();
  return m === 2 ? dt.getUTCDate() >= 21 : m === 5 ? dt.getUTCDate() < 21 : m < 5 && m > 2;
}

function isWinter(birthDate) {
  const dt = new Date(birthDate);
  const m = dt.getMonth();
  return m === 5 ? dt.getUTCDate() >= 21 : m === 8 ? dt.getUTCDate() < 21 : m < 5 && m > 8;
}

function isSpring(birthDate) {
  const dt = new Date(birthDate);
  const m = dt.getMonth();
  return m === 8 ? dt.getUTCDate() >= 21 : m === 11 ? dt.getUTCDate() < 21 : m < 8 && m > 11;
}

function isSummer(birthDate) {
  const dt = new Date(birthDate);
  const m = dt.getMonth();
  return m === 11 ? dt.getUTCDate() >= 21 : m === 2 ? dt.getUTCDate() < 21 : m < 11 && m > 8;
}

function generateGift(birthDate) {
  if(isAutumn(birthDate)) {
    gift = 'buzo';
  }
  if(isWinter(birthDate)) {
    gift = 'sweater';
  }
  if(isSpring(birthDate)) {
    gift = 'camisa';
  }
  if(isSummer(birthDate)) {
    gift = 'remera';
  }
  return gift;
}

module.exports = { generateGift };
