function calculateAge(date) { 
  const birthdate = new Date(date);
  const diff = Date.now() - birthdate.getTime();
  const ageDate = new Date(diff);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}

module.exports = { calculateAge };
