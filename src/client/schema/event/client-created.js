const { DownstreamEvent } = require('ebased/schema/downstreamEvent');
const  Schemy  = require('schemy');

class ClientCreated extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.CLIENT_CREATED',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
        strict: false,
        dni: { type: String, required: true },
        birthDate: { type: String, required: true },
        creditCard : { type: cardSchemy, required: false }
      },
    })
  }
}

const cardSchemy = new Schemy(
  {
    'number': {
      type: String,
      required: true
    },
    'expiration': {
      type: String,
      required: true
    },
    'ccv': {
      type: String,
      required: true
    }
  }
);

module.exports = { ClientCreated };
