const { InputValidation } = require("ebased/schema/inputValidation");

class CreateGiftInputSchema extends InputValidation {
  constructor(payload, meta) {
    super({
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: false,
        dni: { type: String, required: true },
        birthDate: { type: String, required: true }
      }
    });
  }
}

module.exports = { CreateGiftInputSchema };
