const { InputValidation } = require("ebased/schema/inputValidation");
const  Schemy  = require('schemy');

class CreateCardInputSchema extends InputValidation {
  constructor(payload, meta) {
    super({
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: false,
        dni: { type: String, required: true },
        birthDate: { type: String, required: true },
        creditCard : { type: cardSchemy, required: false }
      }
    });
  }
}

const cardSchemy = new Schemy(
  {
    'number': {
      type: String,
      required: true
    },
    'expiration': {
      type: String,
      required: true
    },
    'ccv': {
      type: String,
      required: true
    }
  }
);

module.exports = { CreateCardInputSchema };
