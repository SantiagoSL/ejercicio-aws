const { InputValidation } = require("ebased/schema/inputValidation");

class CreateClientInputSchema extends InputValidation {
  constructor(payload, meta) {
    super({
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: true,
        dni: { type: String, required: true },
        name: { type: String, required: true },
        lastName: { type: String, required: true },
        birthDate: { type: String, required: true }
      }
    });
  }
}

module.exports = { CreateClientInputSchema };
