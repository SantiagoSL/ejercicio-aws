const { InputValidation } = require("ebased/schema/inputValidation");

class UpdateClientInputSchema extends InputValidation {
  constructor(payload, meta) {
    super({
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: false,
        dni: { type: String, required: true },
        name: { type: String, required: false },
        lastName: { type: String, required: false },
        birthDate: { type: String, required: false }
      }
    });
  }
}

module.exports = { UpdateClientInputSchema };
