const { getAllClients } = require('../service/get-all-clients')

module.exports = async (commandPayload, commandMeta) => {
  return {
    body: {
      status: 200,
      message: await getAllClients()
    }
  }
};
