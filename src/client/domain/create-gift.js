const { CreateGiftInputSchema } = require('../schema/input/create-gift');
const { saveGift } = require('../service/create-gift');
const { generateGift } = require('../helper/gift-generator.helper')

module.exports = async (eventPayload, commandMeta) => {
  const client = JSON.parse(eventPayload.Message);
  new CreateGiftInputSchema(client, commandMeta);
  await saveGift(client.dni, generateGift(client.birthDate));
  const response = {
    statusCode: 200,
    body: JSON.stringify('Successfully assigned gift'),
  };
  return response;
};
