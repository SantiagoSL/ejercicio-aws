const { getByDni }  = require('../service/get-by-dni')
const { GetByDniInputSchema }  = require('../schema/input/get-by-dni');

module.exports = async (commandPayload, commandMeta) => {
  new GetByDniInputSchema(commandPayload, commandMeta);
  const client = await getByDni(commandPayload);
  let status;
  let response;
  console.log(client)
  const message = JSON.stringify(client);
  if(client) {
    status = 200;
    response = client
  } else {
    status = 400;
    response = 'client not found';
  }
  return {
    status: status,
    body: response
  };
}