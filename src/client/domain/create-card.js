const { generateCard } = require('../helper/card-generator.helper');
const { CreateCardInputSchema } = require('../schema/input/create-card');
const { saveCard } = require('../service/create-card');
const { calculateAge } = require('../helper/calculate-age.helper');

module.exports = async (eventPayload, commandMeta) => {
  const data = JSON.parse(eventPayload.Message);
  console.log(data)
  new CreateCardInputSchema(data, commandMeta);
  const card = data.creditCard ? data.creditCard : generateCard();
  card.type = calculateAge(data.birthDate) > 45 ? 'gold' : 'classic';
  await saveCard(data.dni, card);
  const response = {
    statusCode: 200,
    body: JSON.stringify('Successfully assigned card'),
  };
  return response;
};
