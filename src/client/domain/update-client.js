const { UpdateClientInputSchema } = require('../schema/input/update-client')
const { updateClient } = require('../service/update-client');
const { publishCreateCardAndGift } = require('../service/create-client');
const { ClientCreated } = require("../schema/event/client-created");
const { getByDni } = require('../service/get-by-dni');

module.exports = async (commandPayload, commandMeta) => {
  new UpdateClientInputSchema(commandPayload, commandMeta);
  const client = await getByDni(commandPayload);
  if(!client) {
    return {
      statusCode: 400,
      body: 'client not found.'
    }
  }
  await updateClient(commandPayload);
  if(commandPayload.birthDate && client.birthDate !== commandPayload) {
    commandPayload.creditCard = client.creditCard;
    await publishCreateCardAndGift(new ClientCreated(commandPayload, commandMeta));
  }
  return {
    statusCode: 200,
    body: 'Succesfully updated client.'
  }
}