const { disableClient }  = require('../service/disable-client')
const { DisableClientInputSchema }  = require('../schema/input/disable-client');
const { getByDni } = require('../service/get-by-dni')

module.exports = async (commandPayload, commandMeta) => {
  new DisableClientInputSchema(commandPayload, commandMeta);
  const client = await getByDni(commandPayload)
  if (!client) {
    return {
      body: {
        status: 400,
        message: 'client not found.'
      }
    }
  }
  await disableClient(commandPayload.dni);
  return {
    body: {
      status: 200,
      message: 'Successfully disabled client.'
    }
  }
}