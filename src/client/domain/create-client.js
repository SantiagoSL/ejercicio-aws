const { CreateClientInputSchema } = require("../schema/input/create-client");
const { ClientCreated } = require("../schema/event/client-created");
const { createClient, publishCreateCardAndGift } = require("../service/create-client");

module.exports = async (commandPayload, commandMeta) => {
  new CreateClientInputSchema(commandPayload, commandMeta);
  await createClient(commandPayload);
  await publishCreateCardAndGift(new ClientCreated(commandPayload, commandMeta));

  return {
    body: {
      status: 200,
      message: 'Successfully created client.'
    }
  }
};
