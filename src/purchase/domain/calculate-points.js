const { getByDni } = require('../../client/service/get-by-dni');
const { savePoints } = require('../service/calculate-points')
const { CalculatePointsInputSchema } = require('../schema/input/calculate-points');
const { calculatePoints } = require('../helper/calculate-points')

module.exports = async (commandPayload, commandMeta) => {
  new CalculatePointsInputSchema(commandPayload, commandMeta);
  const client = await getByDni(commandPayload)
  const actualPoints = client.points ? client.points : 0;
  let finalPrice = 0;
  commandPayload.products.forEach(product => {
    finalPrice += product.finalPrice;
  });
  commandPayload.points = actualPoints + calculatePoints(finalPrice);
  await savePoints(commandPayload);
  const response = {
    statusCode: 200,
    body: JSON.stringify('Success'),
  };
  return response;
};
