const { CreatePurchase } = require('../schema/input/create-purchase');
const { createPurchase, emitPurchaseCreated } = require('../service/create-purchase');
const { getByDni } = require('../../client/service/get-by-dni');
const { PurchaseCreatedEvent } = require('../schema/event/purchase-created');
const { calculateFinalPrice } = require('../helper/calculate-final-price')

const uuid = require('uuid');

module.exports = async (commandPayload, commandMeta) => {
  new CreatePurchase(commandPayload, commandMeta);
  const client = await getByDni(commandPayload);
  if (client.disabled || !client.creditCard || !client.creditCard.type) {
    return {
      body: {
        status: 409,
        message: 'Invalid client.'
      }
    }
  }
  commandPayload.dni = client.dni;
  commandPayload.id = uuid.v4();
  commandPayload.products = calculateFinalPrice(commandPayload.products, client.creditCard.type);
  await createPurchase(commandPayload);
  await emitPurchaseCreated(new PurchaseCreatedEvent(commandPayload, commandMeta));

  return {
    body: {
      status: 200,
      message: 'Successfully created purchase.'
    }
  }
};
