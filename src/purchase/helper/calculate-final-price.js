function calculateFinalPrice(products, cardType) {
  products.forEach(product => {
    const discoutRate = cardType === 'gold' ?  0.12  : 0.08
    const discount = (product.price * discoutRate);
    product.finalPrice = product.price - discount;
  });
  return products;
}

module.exports = { calculateFinalPrice };