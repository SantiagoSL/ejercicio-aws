const dynamo = require('ebased/service/storage/dynamo');

const savePoints = async (commandPayload) => {
  const dbParams = {
    TableName: process.env.CLIENTS_TABLE,
    UpdateExpression: "SET #points=:p",
    Key: { dni: commandPayload.dni },
    ExpressionAttributeNames: {
      '#points': 'points'
    },
    ExpressionAttributeValues: {
      ':p': commandPayload.points
    },
  }
  await dynamo.updateItem(dbParams);
}

module.exports = { savePoints };
