const dynamo = require('ebased/service/storage/dynamo');
const sqs = require('ebased/service/downstream/sqs');

const createPurchase = async (item) => {
  await dynamo.putItem({ TableName: process.env.PURCHASE_TABLE, Item: item});
}

const emitPurchaseCreated = async (purchaseCreatedEvent) => {
  const { eventPayload, eventMeta } = purchaseCreatedEvent.get();
  const sqsSendParams = {
    QueueUrl: process.env.CALCULATE_POINTS_QUEUE,
    MessageBody: eventPayload,
  };
  await sqs.send(sqsSendParams, eventMeta);
}

module.exports = { createPurchase, emitPurchaseCreated };