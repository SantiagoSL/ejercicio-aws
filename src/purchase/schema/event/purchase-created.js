const { DownstreamEvent } = require('ebased/schema/downstreamEvent');
const  Schemy  = require('schemy');

class PurchaseCreatedEvent extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'PURCHASE_CREATED_EVENT',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
        strict: false,
        id: { type: 'uuid/v4', required: true },
        dni: { type: String, required: true },
        products: { type: productSchemy, required: true }
      },
    })
  }
}

const productSchemy = new Schemy([
  {
    'product': {
      type: String,
      required: true
    },
    'price': {
      type: String,
      required: true
    }
  }
]);

module.exports = { PurchaseCreatedEvent };