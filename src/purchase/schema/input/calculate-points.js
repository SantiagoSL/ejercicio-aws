const { InputValidation } = require("ebased/schema/inputValidation");
const  Schemy  = require('schemy');

class CalculatePointsInputSchema extends InputValidation {
  constructor(payload, meta) {
    super({
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: false,
        dni: { type: String, required: true },
        products: { type: finalProductSchemy, required: true }
      }
    });
  }
}

const finalProductSchemy = new Schemy([
  {
    'product': {
      type: String,
      required: true
    },
    'price': {
      type: String,
      required: true
    },
    'finalPrice': {
      type: Number,
      required: true
    }
  }
]);



module.exports = { CalculatePointsInputSchema };