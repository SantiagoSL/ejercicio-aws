const { InputValidation } = require("ebased/schema/inputValidation");
const  Schemy  = require('schemy');

class CreatePurchase extends InputValidation {
  constructor(payload, meta) {
    super({
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: true,
        dni: { type: String, required: true },
        products: { type: productSchemy, required: true }
      }
    });
  }
}

const productSchemy = new Schemy([
  {
    'product': {
      type: String,
      required: true
    },
    'price': {
      type: String,
      required: true
    }
  }
]);

module.exports = { CreatePurchase };